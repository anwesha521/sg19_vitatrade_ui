import * as Highcharts from 'highcharts';
import { Injectable } from '@angular/core';

@Injectable()
export class HighchartsService {
    constructor() {
    }

    createChart(el, cfg) {     
      return Highcharts.chart(el, cfg);
    }

    deleteChart(chart: Highcharts.Chart){
      chart.destroy();
    }

    addSeries(chart: Highcharts.Chart,cfg){
      chart.addSeries(cfg);
    }

}