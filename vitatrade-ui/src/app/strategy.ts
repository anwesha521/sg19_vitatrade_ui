export class Strategy{
    strategyId: number;
    strategyDescription: string;
    


    constructor(id:number, strategyDesc: string){
        this.strategyId = id;
        this.strategyDescription = strategyDesc;
    }
}