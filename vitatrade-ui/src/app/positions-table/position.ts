import { Stock } from '../stocks';

export interface Position { // POsition is synonymous with Strategy in back-end
    // in front end Strategy interface is used to get the different types of strategies available to the trader

    //**** PLACEHOLDER INTERFACE ****//

    stratID: number;
    strategy: string;
    stockID: Stock;
    volume: number;
    pnl: number;
    roi: number;
    active: boolean; 
    sleepStatus: boolean;


}

 