import { Component, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-stop-strategy-modal',
  templateUrl: './stop-strategy-modal.component.html',
  styleUrls: ['./stop-strategy-modal.component.css']
})
export class StopStrategyModalComponent implements OnInit {

  constructor(public activeModal: NgbActiveModal) { }

  ngOnInit() {
  }

}
