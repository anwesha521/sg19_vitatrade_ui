import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StopStrategyModalComponent } from './stop-strategy-modal.component';

describe('StopStrategyModalComponent', () => {
  let component: StopStrategyModalComponent;
  let fixture: ComponentFixture<StopStrategyModalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StopStrategyModalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StopStrategyModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
