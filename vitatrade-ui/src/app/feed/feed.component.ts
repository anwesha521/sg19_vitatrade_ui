import { Component, OnInit, NgModule, ViewChild, ElementRef } from '@angular/core';
import { HighchartsService } from '../highcharts.service'
import {ConfigService} from '../config.service'
import {formatDate } from '@angular/common';
import { isEmptyExpression } from '@angular/compiler';
import { color } from 'highcharts';
import { nextTick } from 'q';

@Component({
  selector: 'app-feed',
  templateUrl: './feed.component.html',
  styleUrls: ['./feed.component.css']
})
export class FeedComponent implements OnInit {

  
  @ViewChild('charts', {static: true}) public chartEl: ElementRef;
  feedUrl: string;
  chartData: any[];
  currData: any;
  series: any;
  selectedOption: string;
  myOptions: any;
  chart: any;

  stocks = ["goog","nsc","aapl","ALL"];
  constructor(private highcharts: HighchartsService, private configService: ConfigService) {}
  ngOnInit() {
    this.selectedOption='goog';
    this.selectedItemChanged()
  }

multipleSeriesAdd(){
    var thiObj = this;
    
    if(this.chart!=null){
      this.chart.destroy();
    } 
 
      var myOptionsTemp =
      {
        chart: {
          backgroundColor:'#3E3D32',
        events: {
          
          load: function () {
  
    
            // set up the updating of the chart each second          
            var series = this.series[0];
            
            setInterval(() => {
              var x = (new Date()).getTime(); // current time
              thiObj.configService.getConfig(thiObj.stocks[0])
              .subscribe(response =>
                {
  
                 this.currData=response;
                });
              var y =this.currData;
              try{
              series.addPoint([x, y], true, true);
              }
              catch(e){
                
              }
                   
            }, 1000);
          }
        }
      },
      labels: {
        enabled: false
    },
    xAxis: {
      
      type: 'datetime',
      labels: {
        style: {
          color: 'white'
        },
          formatter: function () {
              return formatDate(this.value, 'hh:mm:ss a', 'en-US', '+0530')
          }
      }
    },
        yAxis: {
          labels:{
          style: {
            color: 'white'
          }
          },
          lineWidth: 0,
          minorGridLineWidth: 0,
          gridLineColor: 'transparent',
          lineColor: 'transparent'
        },
  
            time: {
              useUTC: false
            },
        
            title: {
              style: {
                color: 'white'
              },
              text: 'Live Feed'
              
            },
          
            exporting: {
              enabled: false
            },
          
            series: [{
              name: 'Prices for stock '+thiObj.stocks[0],
              yAxis:0,
              data: (function () {
                // generate an array of random data
            
                var i,time = (new Date()).getTime();
                var tempdata = [];
  
                for (i = -10; i <= 0; i += 1) {
                  tempdata.push([
                    time + i * 1000,
                    600
                  ]);
                }
              return tempdata;
              }())
            }]
          };  

   this.chart= this.highcharts.createChart(this.chartEl.nativeElement,myOptionsTemp);
   var thiObj = this;
   

    this.stocks.forEach( (s) => {
      if(s!='ALL' && s!=thiObj.stocks[0]){
   var tempseries= {
    name: 'Prices for stock '+s,
    data: (function () {
      // generate an array of random data
      var i,time = (new Date()).getTime();
      var tempdata = [];

      for (i = -10; i <= 0; i += 1) {
        tempdata.push([
          time + i * 1000,
          600
        ]);
      }
    return tempdata;
    }())
  };
  thiObj.chart.addSeries(tempseries);
  thiObj.chart.redraw();
    }
    });
  thiObj=this;
  
 
    setInterval(function () {   
    var count=0;
          for (var j in  thiObj.chart.series) {
              if(count==0){
                count++;
                continue;
              }
              count++;
              var series =  thiObj.chart.series[j];
              var x = (new Date()).getTime(); // current time
              thiObj.configService.getConfig(thiObj.stocks[count])
              .subscribe(response =>
                {
  
                 this.currData=response;
                });
              var y =this.currData;
              series.addPoint([x,y], true,  series.data.length >= 10);
          }
        
      }, 1000);

}

  selectedItemChanged(){
    if(this.selectedOption=="ALL"){
      this.multipleSeriesAdd();
      return;
    }
    var thiObj = this;
    this.myOptions=null;
    this.myOptions =
    {
      chart: {
        backgroundColor:'#3E3D32',
      events: {
        
        load: function () {

  
          // set up the updating of the chart each second          
          var series = this.series[0];
          
          setInterval(() => {
            var x = (new Date()).getTime(); // current time
            thiObj.configService.getConfig(thiObj.selectedOption)
            .subscribe(response =>
              {

               this.currData=response;
              });
            var y =this.currData;
            try{
            series.addPoint([x, y], true, true);
            }
            catch(e){
              
            }
            
            
            
          }, 1000);
        }
      }
    },
    labels: {
      enabled: false
  },
  xAxis: {
    
    type: 'datetime',
    labels: {
      style: {
        color: 'white'
      },
        formatter: function () {
            return formatDate(this.value, 'hh:mm:ss a', 'en-US', '+0530')
        }
    }
  },
      yAxis: {
        labels:{
        style: {
          color: 'white'
        }
        },
        lineWidth: 0,
        minorGridLineWidth: 0,
        gridLineColor: 'transparent',
        lineColor: 'transparent'
      },

          time: {
            useUTC: false
          },
      
          title: {
            style: {
              color: 'white'
            },
            text: 'Live Feed'
            
          },
        
          exporting: {
            enabled: false
          },
        
          series: [{
            name: 'Prices for stock '+thiObj.selectedOption,
            data: (function () {
              // generate an array of random data
          
              var i,time = (new Date()).getTime();
              var tempdata = [];

              for (i = -10; i <= 0; i += 1) {
                tempdata.push([
                  time + i * 1000,,
                  600
                ]);
              }
            console.log(tempdata);
            return tempdata;
            }())
          }]
        };

    if(this.chart!=null){
      this.chart.destroy();
      //this.highcharts.deleteChart(this.chart);
    }    
    this.chart= this.highcharts.createChart(this.chartEl.nativeElement, this.myOptions);
  }

  }
