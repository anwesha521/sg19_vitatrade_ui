import { Component, OnInit, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Stock } from '../stocks';
import { StrategyService } from '../strategy.service';
import { Strategy } from '../strategy';

@Component({
  selector: 'app-trade-stock-modal',
  templateUrl: './trade-stock-modal.component.html',
  styleUrls: ['./trade-stock-modal.component.css']
})

export class TradeStockModalComponent implements OnInit {

  chosenStrategy = "none";
  stockAmount=100;
  stockLimit = 1000000;
  //Moving Average
  longAverage = 60;
  shortAverage= 15;
  crossoverTime = 1;
  //Price Breakout
  periodTime = 2; // minimum of 2 seconds
  breakoutNumber = 1; 
  // Bollinger Bands
  averagePeriod = 60;
  multipleOfSd = 2;


  //tradingStrategies=["Moving Average","Price Breakout"]
  @Input() stock: Stock; // to determine the stock chosen
  //constructor(public activeModal: NgbActiveModal, private strategyService: StrategyService) {}

  constructor(public activeModal: NgbActiveModal,  private strategyService: StrategyService) {}

  ngOnInit() {
  }

  onSelectionChange(entry: string){
    if (entry ==null){
      entry = "none";
    } 
    this.chosenStrategy = entry;
    console.log('TESTING');
    console.log(typeof(this.longAverage));
    console.log(this.chosenStrategy);
  }

  onSubmit(){
    if (this.chosenStrategy=='Moving Average'){
      // submit the strategy as a stringifeid JSON becasue sending Json not working IDK WHY
      let strategyString:String=`{
        stockCode: ${this.stock.stockCode},
        strategyDescription: ${this.chosenStrategy},
        shortAverage:  ${this.shortAverage},
        longAverage:  ${this.longAverage},
        crossoverTime:  ${this.crossoverTime},
        stockAmount: ${ this.stockAmount},
        stockLimit:  ${this.stockLimit}   
      }`;
      this.strategyService.startStrategy(strategyString);
                      
    } else if (this.chosenStrategy=='Price Breakout'){
      let strategyString: String = `{
        stockCode: ${this.stock.stockCode},
        strategyDescription: ${this.chosenStrategy},
        periodTime:  ${this.periodTime},
        breakoutNumber:  ${this.breakoutNumber},
        stockAmount: ${ this.stockAmount},
        stockLimit:  ${this.stockLimit}   
      }`;

      this.strategyService.startStrategy(strategyString);
                  
    }  else if (this.chosenStrategy=='Bollinger Bands'){
      let strategyString: String = `{
        stockCode: ${this.stock.stockCode},
        strategyDescription: ${this.chosenStrategy},
        stockLimbb: ${this.averagePeriod},
        multipleOfSd:  ${this.multipleOfSd},
        stockAmount:  ${this.stockAmount},
        stockLimit: ${ this.stockLimit}          
      }`;

      this.strategyService.startStrategy(strategyString);
                  
    }else{
      console.error('Strategy chosen is not in one of the options provided')
    }
    
  }

  invalidInput(){
    return (
      this.stockAmount > this.stockLimit ||
      this.shortAverage>this.longAverage ||
      this.crossoverTime<1 ||
      this.periodTime<1 ||
      this.breakoutNumber<1 ||
      this.periodTime<2 || 
      this.averagePeriod<2)

  }
  
  

}
