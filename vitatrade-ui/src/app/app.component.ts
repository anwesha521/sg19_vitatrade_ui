import { Component } from '@angular/core';
import { OrdersTableComponent } from  './orders-table/orders-table.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  title = 'Vitatrade'; 
}
